# AutoMoG 3D - A framework for automated data-driven modeling and optimization of multi-energy systems

AutoMoG 3D has been developed by members of the EPSE group in the following papers:

> [Kämper, A., Leenders, L., Bahl, B., & Bardow, A. (2021). AutoMoG: Automated data-driven Model Generation of multi-energy systems using piecewise-linear regression. Computers & Chemical Engineering, 145, 107162](https://doi.org/10.1016/j.compchemeng.2020.107162)

> [Kämper, A., Holtwerth, A., Leenders, L., & Bardow, A. (2021). AutoMoG 3D: Automated Data-Driven Model Generation of Multi-Energy Systems Using Hinging Hyperplanes. Frontiers in Energy Research, 9, 719658](https://doi.org/10.3389/fenrg.2021.719658)

You can find the corresponding Git here: 

https://git-ce.rwth-aachen.de/ltt/automog-3d
